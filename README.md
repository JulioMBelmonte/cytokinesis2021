# Cytokinesis2021

CytoSim configuration file to run the simulations published in the article "­Plastin and β-Heavy-spectrin cooperate to stabilize the actomyosin cortex during cytokinesis" by Sobral, Chen et al. Current Biology, 2021.

doi:

## Getting started

 This configuration file refers to the reference simulation with all 4 elements (Actin Filaments,Myosin motors, Plastin and Beta-H-Spectrin).
 Other cases can be simulated from this base file by adjusting the corresponding number of elements and size of the ring.


## Cytosim version

This configuration file must be run with CytoSim forked version [gitlab.com/JulioMBelmonte/cytosim](gitlab.com/JulioMBelmonte/cytosim)

## Authors 

Michael J. Norman and Julio M. Belmonte

Dept of Physics, NCSU

Quantitative and Computational Developmental Biology Cluster

12 August, 2021
